# Sample API-App

## Build Image
```
cd ./sample-api
docker build . -f Dockerfile -t bhough01/canary-api
docker push bhough01/canary-api
```

## Test
```
docker run -d -p 3000:3000 bhough01/canary-api
docker ps
docker delete <container id> -f
```

## Deploy into minikube
```
kubectl apply -f deployment.yaml
minikube service nodeapp-service
```

## Deploy into EKS 
# Sample Web-App

## Setup
```
cd ./apps/web/
docker build ./apps/web -f ./apps/web/Dockerfile -t bhough01/canary-app_web
docker push bhough01/canary-app_web
helm install canary-app ./helm/canary-app/
```